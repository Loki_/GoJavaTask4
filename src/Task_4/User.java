package Task_4;

/**
 * Created by Loki_ on 13.11.16.
 */
public class User {

    private String name;
    private int balance;
    private int monthsOfEmployment;
    private String companyName;
    private int salary;
    private Bank bank;

    public User(String name, int balance, int monthsOfEmployment, String companyName, int salary, Bank bank){
        this.name = name;
        this.balance = balance;
        this.monthsOfEmployment = monthsOfEmployment;
        this.companyName = companyName;
        this.salary = salary;
        this.bank = bank;
    }

    public String getName(){
        return this.name;
    }
    public void setName(String name){
        this.name = name;
    }

    public int getBalance(){
        return this.balance;
    }
    public void setBalance(int balance){
        this.balance = balance;
    }

    public int getMonthsOfEmployment(){
        return this.monthsOfEmployment;
    }
    public void setMonthsOfEmployment(int monthsOfEmployment){
        this.monthsOfEmployment = monthsOfEmployment;
    }

    public String getCompanyName(){
        return this.companyName;
    }
    public void setCompanyName(String companyName){
        this.companyName = companyName;
    }

    public int getSalary(){
        return this.salary;
    }
    public void setSalary(int salary){
        this.salary = salary;
    }

    public Bank getBank(){
        return this.bank;
    }
    public void setBank(Bank bank){
        this.bank = bank;
    }

    @Override
    public String toString(){
        String e  = " a + b";
        return e;
    }
}
