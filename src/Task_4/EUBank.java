package Task_4;

/**
 * Created by Loki_ on 17-Nov-16.
 */
public class EUBank extends Bank {
    public EUBank(long id, String bankCountry, CurrencyE currency, int numberOfEmployees, double avrSalaryOfEmployee, long rating, long totalCapital) {
        super(id, bankCountry, currency, numberOfEmployees, avrSalaryOfEmployee, rating, totalCapital);
    }

    @Override
    int getLimitOfWithdrawal() {
        int cash = 0;
        if (currency == CurrencyE.USD){
            cash = 2000;
        } else if (currency == CurrencyE.EUR) {
            cash = 2200;
        }
        return cash;
    }

    @Override
    int getLimitOfFunding() {
        int cash = 0;
        if (currency == CurrencyE.EUR) {
            cash = 20000;
        } else if (currency == CurrencyE.USD) {
            cash = 10000;
        }
        return cash;
    }

    @Override
    int getMonthlyRate() {
        int rate = 0;
        if (currency == CurrencyE.USD) {
            rate = 0;
        } else if (currency == CurrencyE.EUR) {
            rate = 1;
        }
        return rate;
    }

    @Override
    int getCommission(int summ) {
        int commission = 0;
        if (currency == CurrencyE.USD && summ < 1000){
            commission = 5;
        } else if (currency == CurrencyE.USD && summ >= 1000) {
            commission = 7;
        } else if (currency == CurrencyE.EUR && summ < 1000) {
            commission = 2;
        } else if (currency ==  CurrencyE.EUR && summ >= 1000) {
            commission = 4;
        }
        return commission;
    }
}
