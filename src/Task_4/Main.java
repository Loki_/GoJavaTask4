package Task_4;

/**
 * Created by anna on 20.11.16.
 */
public class Main {
    public static void main(String[] args) {

        USBank usBank1 = new USBank(98765, "USA", CurrencyE.USD, 12, 1200.0, 12345, 998878);
        USBank usBank2 = new USBank(78953, "USA", CurrencyE.EUR, 13, 1300.0, 12345, 998878);
        EUBank euBank1 = new EUBank(34856, "Germany", CurrencyE.USD, 14, 1500.0, 12345, 999778);
        EUBank euBank2 = new EUBank(12997, "Germany", CurrencyE.EUR, 15, 1600.0, 12345, 999778);
        ChinaBank chinaBank1 = new ChinaBank(88731, "China", CurrencyE.USD, 16, 1350.5, 12345, 667778);
        ChinaBank chinaBank2 = new ChinaBank(44501, "China", CurrencyE.EUR, 17, 1450.5, 12345, 667778);


        User userUSB1 = new User("Mark", 15000, 24, "Apple", 3500, usBank1);
        User userUSB2 = new User("Tegan", 13000, 36, "Google", 5000, usBank2);
        User userEUB1 = new User("Derek", 25000, 12, "Samsung", 2600, euBank1);
        User userEUB2 = new User("Meredit", 38000, 18, "Memorial Hospital", 3400, euBank2);
        User userCB1 = new User("Yoko", 1450, 3, "Super Market", 1200, chinaBank1);
        User userCB2 = new User("Sam", 45000, 6, "Uber", 4000, chinaBank2);

        System.out.println(userCB1.getBalance());
        System.out.println(userCB2.getBank());
        System.out.println(userEUB1.getCompanyName());
        System.out.println(userEUB2.getSalary());
        System.out.println(userUSB1.getName());
        System.out.println(userUSB2.getMonthsOfEmployment());

    }
}
