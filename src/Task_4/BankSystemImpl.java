package Task_4;

/**
 * Created by anna on 20.11.16.
 */
public class BankSystemImpl implements BankSystem {
    @Override  // лимит снятия
    public void withdrawOfUser(User user, int amount) {
        int balance = user.getBalance();
        if (balance > amount){
            int cash = balance - amount;
        }
    }

    @Override  // лимит пополнения
    public void fundUser(User user, int amount) {
        int balance = user.getBalance();
        int money = balance + amount;
    }

    @Override   //
    public void transferMoney(User fromUser, User toUser, int amount) {
        int fromUserBalance = fromUser.getBalance();
        int toUserBalance = toUser.getBalance();
        if (fromUserBalance > amount){
            toUserBalance += amount;
        }
    }

    @Override   //
    public void paySalary(User user) {
        int balance = user.getBalance();
        int paySalary = user.getSalary();
        int newBalance = balance + paySalary;
    }
}
